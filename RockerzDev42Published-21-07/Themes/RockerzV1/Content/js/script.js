
function leftNavActive() {
	var url = this.location.href;
	var activePage = url.substring(url.lastIndexOf('/') + 1);
	$('.block ul.list.list2 a').each(function () {
		var currentPage = this.href.substring(this.href.lastIndexOf('/') + 1);
		$(this).removeClass('active');
		if (activePage == currentPage) {
			if (!$(this).hasClass('active')) {
				$(".block ul.list.list2 a.active").removeClass("active");
				$(this).addClass("active");
			}
		}
	});
}


(function () {

	var stickyTopSections = $('.checkOutC1');

	var stickyTop = function () {
		var scrollTop = $(window).scrollTop();
		$('.checkOutC1').each(function () {
			var $this = $(this);
			if (scrollTop > 200 && scrollTop < 500) {
				$this.addClass('sticky');
			}
			else {
				$this.removeClass('sticky');
			}
		});
	};

	stickyTop();

	$(window).scroll(function () {
		stickyTop();
	});

});

 function addressTypeNav() {
	$(".address_type ul li").first().addClass('active');
	$('[name=addressType]').click(function () {
		var value = $(this).val();
		$('[name=addressType]').removeClass('active');
		$(this).addClass('active');
		$('#@Html.IdFor(x=>x.AddressTypeId)').val(value);
		$(this).parents('li').addClass('active').siblings().removeClass('active');
	});                      
} 

function attributeSelector() {
	var springColorSelector = '.springcontrol .spring-color-box'; 
	$(springColorSelector).on('click', function () {
		$(springColorSelector).removeClass('active');
		$(this).addClass('active');
	});
	var randomizeSelector = '.randomcontrol';
	$(randomizeSelector).on('click', function () {
		$(randomizeSelector).removeClass('active');
		$(this).addClass('active');
	});
};
function onlyNumberKey(evt) {
	// Only ASCII charactar in that range allowed
	var ASCIICode = (evt.which) ? evt.which : evt.keyCode
	if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
		return false;
	return true;
}
function alphaOnly(event) {
	var key = event.keyCode;
	return ((key > 64 && key < 91) || (key > 96 && event.charCode < 123) || (key == 32));
};
