﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />
/// <reference path="~/Content/Scripts/rockerz.common.js" />

var clickedRockerz = "rock_front_left";
var rockerzLeftRight = "left";
var bgImageColor = "white";
var wheelColorId = 0;

$(document).ready(function () {
    randomlyChooseColorsAndSprings();
	
	var color;
    $('area[id^="maparea"]').mouseover(function () {
        color = $(this).attr("color");
		//alert('url("/Content/Images/design/' + rockerzLeftRight + '_' + color);
		$("#" + clickedRockerz).css('background-image', 'url("/Content/Images/design/' + rockerzLeftRight + '_' + color + '.png")');
        showColorName(color, clickedRockerz);
    });
    $('area[id^="maparea"]').mouseout(function () {
    });
    $('area[id^="maparea"]').click(function () {
        clickedRockerz = null;
        return false;
    });

    $("#spring_silver_outer").click(function () {
        changeSpringColor("silver");
        return false;
    });
    $("#spring_gold_outer").click(function () {
        changeSpringColor("gold");
        return false;
    });

    $("#designmine_main").click(function () {
        if (bgImageColor == "white") {
            $("#rock_main").css('background-image', 'url("/Content/Images/design/design_bg_black.png")');
            bgImageColor = "black";
        } else {
            $("#rock_main").css('background-image', 'url("/Content/Images/design/design_bg_grey.png")');
            bgImageColor = "white";
        }
    });
    $("#rock_front_left").click(function () {
        clickedRockerz = $(this).attr("id");
		
        rockerzLeftRight = "left";
        return false;
    });
    $("#rock_front_right").click(function () {
        clickedRockerz = $(this).attr("id");
		
        rockerzLeftRight = "right";
        return false;
    });
    $("#rock_back_left").click(function () {
        clickedRockerz = $(this).attr("id");
		
        rockerzLeftRight = "left";
        return false;
    });
    $("#rock_back_right").click(function () {
        clickedRockerz = $(this).attr("id");
		
        rockerzLeftRight = "right";
        return false;
    });

    $("#rock_front_left, #rock_back_left").mousewheel(function (event, delta) {
        mouseWheelThroughColors(this, event, delta, "left");
        return false;
    });
    $("#rock_front_right, #rock_back_right").mousewheel(function (event, delta) {
        mouseWheelThroughColors(this, event, delta, "right");
        return false;
    });

    $("#design_randomize_btn").click(function () {
        randomlyChooseColorsAndSprings();
        return false;
    });
    $("#design_start_btn").click(function () {
        $("#design_bubble_1").toggle();
        $("#design_bubble_2").toggle();
        $("#design_bubble_3").toggle();
        return false;
    });
    $("#design_finalize_btn").click(function () {
        $("#dlg_ecommerce_coming_soon_form").dialog("open");
        return false;
    });

    $("#dlg_ecommerce_coming_soon_form").dialog({
        autoOpen: false,
        height: "auto",
        width: "auto",
        modal: true,
        resizable: false,
        buttons: {
            "Open Order Form": function () {
                popupCenterPdf('/Content/PDF/RockerzOnlineIndividualOrderForm.pdf', 'Online Order Form');
                $(this).dialog("close");
            },
            "Contact Us": function () {
                window.location = '/Home/Company?t=c';
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            $("#ecommerce_coming_soon_form").get(0).reset();
            $("#ecommerce_coming_soon_form").validate().resetForm();
            $("#ecommerce_coming_soon_form div.formerror").hide();
        }
    });

    /*
    $("#dlg_add_to_cart_form").dialog({
    autoOpen: false,
    height: "auto",
    width: "auto",
    modal: true,
    resizable: false,
    buttons: {
    "Add To Cart": function () {
    $("#add_to_cart_form").submit();
    },
    Cancel: function () {
    $(this).dialog("close");
    }
    },
    close: function () {
    $("#add_to_cart_form").get(0).reset();
    $("#add_to_cart_form").validate().resetForm();
    $("#add_to_cart_form div.formerror").hide();
    }
    });
    // validate and submit form
    $("#add_to_cart_form").validate({
    onfocusout: false,
    errorClass: "addToCartError",
    rules: {
    manufacturer: { required: true },
    blademodel: { required: true },
    bladesize: { required: true }
    },
    messages: {
    manufacturer: { required: "A manufacturer is required" },
    blademodel: { required: "A blade model is required" },
    bladesize: { required: "A blade size is required" }
    },
    submitHandler: function () {
    $("#add_to_cart_form div.formerror").hide();
    submitAddToCartForm_Request();
    $("#dlg_add_to_cart_form").dialog("close");
    },
    invalidHandler: function (e, validator) {
    var errors = validator.numberOfInvalids();
    if (errors) {
    var message = "There are missing or invalid fields. They have been highlighted below.";
    $("#add_to_cart_form div.formerror span").html(message);
    $("#add_to_cart_form div.formerror").show();
    } else {
    $("#add_to_cart_form div.formerror").hide();
    }
    }
    });

    $("#addtocart_manufacturer").change(function () {
    var make = $(this).val();
    loadAddToCartBladeModels_Request(make);
    });
    $("#addtocart_blademodel").change(function () {
    var make = $("#addtocart_manufacturer").val();
    var model = $(this).val();
    loadAddToCartBladeSizes_Request(make, model);
    });
    $("#addtocart_bladesize").change(function () {
    var make = $("#addtocart_manufacturer").val();
    var model = $("#addtocart_blademodel").val();
    var size = $(this).val();
    loadAddToCartBladeDetails_Request(make, model, size);
    });
    selectFirstOption("addtocart_manufacturer");
    */
});

function selectFirstOption(selectId) {
    $("#" + selectId).val($("#" + selectId + " option:first").val());
    $("#" + selectId).trigger("change");
}

// *** Submit Add To Cart Form (Request/Response/Error)
//#region
function submitAddToCartForm_Request() {
    var qs = $("#add_to_cart_form").serialize();
    var url = "/HomeJson/AddToCart";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: qs,
        success: submitAddToCartForm_Response,
        error: submitAddToCartForm_Error
    });
}
function submitAddToCartForm_Response(data) {
    if (data.Success) {
        alert(data.Data);
    } else {
        alert(data.Data);
    }
}
function submitAddToCartForm_Error(xhr) {
    showAjaxErrorMessage("submitAddToCartForm_Error", xhr.status, xhr.response);
}
//#endregion

function mouseWheelThroughColors(rockerzId, event, delta, leftRight) {
	
    var colors = getColors();
    wheelColorId = (delta > 0) ? wheelColorId + 1 : wheelColorId - 1;
    if (wheelColorId < 0) {
        wheelColorId = colors.length - 1;
    } else if (wheelColorId > colors.length - 1) {
        wheelColorId = 0;
    }
    var color = colors[wheelColorId];
    $(rockerzId).css('background-image', 'url("/Content/Images/design/' + leftRight + '_' + color + '.png")');
    showColorName(color, $(rockerzId).attr("id"));
}

function changeSpringColor(toColor) {
    var arrSpr = ["rock_back_spring", "rock_front_spring"];
    if (toColor == "gold") {
        $.each(arrSpr, function (index, value) {
            $("#" + value).css('background-image', 'url("/Content/Images/design/gold.png")');
        });
        $("#spring_silver_name").css("font-weight", "normal");
        $("#spring_gold_name").css("font-weight", "bold");
    } else { // toColor == "silver"
        $.each(arrSpr, function (index, value) {
            $("#" + value).css('background-image', 'url("/Content/Images/design/silver.png")');
        });
        $("#spring_silver_name").css("font-weight", "bold");
        $("#spring_gold_name").css("font-weight", "normal");
    }
}

function randomlyChooseColorsAndSprings() {
	
    var randomNum;
    var rockerz = { "rock_front_left": "left", "rock_front_right": "right", "rock_back_left": "left", "rock_back_right": "right" };
    var colors = getColors();
    $.each(rockerz, function (rock, lr) {
        randomNum = Math.floor(Math.random() * colors.length);
        $("#" + rock).css('background-image', 'url("/Content/Images/design/' + lr + '_' + colors[randomNum] + '.png")');
        showColorName(colors[randomNum], rock);
    });
    var springColors = ["silver", "gold"];
    randomNum = Math.floor(Math.random() * springColors.length);
    changeSpringColor(springColors[randomNum]);
}

function getColors() {
	
    var colors = ["Bahama Blue", "Berry Sorbet", "Extreme Green", "Layback Lilac", "Lemon Ice", "Pacific Blue", "Peppermint Pink", "Purple Passion", "Racer Red", "Sublime Lime", "Tangerine Tango", "Tuxedo Black"];
    return colors;
}
function getColorNames() {
    var colors = ["bahama blue", "berry sorbet", "extreme green", "layback lilac", "lemon ice", "pacific blue", "peppermint pink", "purple passion", "racer red", "sublime lime", "tangerine tango", "tuxedo black"];
    return colors;
}
function getColorValues() {
    var colors = ["#043885", "#9A2144", "#71CF41", "#B98DAD", "#EEDB43", "#2BA4CF", "#FF307E", "#5C367D", "#B30F21", "#D2EF50", "#FF9F26", "#000000"];
    return colors;
}

function showColorName(color, rock) {
	
    var colors = getColors();
    var colorNames = getColorNames();
    var colorValues = getColorValues();
    var idx = $.inArray(color, colors);
    var text = colorNames[idx];
    var cssColor = colorValues[idx];
    $("#" + rock + "_colorname span").text(text);
    $("#" + rock + "_colorname span").css("color", cssColor);
}

// *** loadAddToCartBladeModels (Request/Response/Error) ***
function loadAddToCartBladeModels_Request(make) {
    var qs = { make: make };
    var url = "/HomeJson/CutPointsAllModels";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: qs,
        success: loadAddToCartBladeModels_Response,
        error: loadAddToCartBladeModels_Error
    });
}
function loadAddToCartBladeModels_Response(data) {
    if (data.Success) {
        $("#addtocart_blademodel").scrollTop(0);
        $("#addtocart_blademodel").find("option").remove().end();
        if (data.Data != null) {
            $.each(data.Data, function () {
                $("#addtocart_blademodel").append('<option value="' + this + '">' + this + '</option>');
            });
        }
        selectFirstOption("addtocart_blademodel");
    } else {
        $("#admin_alert_msg").text(data.Data);
        $("#dlg_admin_alert").dialog("open");
    }
}
function loadAddToCartBladeModels_Error(xhr) {
    showAjaxErrorMessage("loadAddToCartBladeModels_Error", xhr.status, xhr.response);
}

// *** loadAddToCartBladeSizes (Request/Response/Error) ***
function loadAddToCartBladeSizes_Request(make, model) {
    var qs = { make: make, model: model };
    var url = "/HomeJson/CutPointsAllSizes";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: qs,
        success: loadAddToCartBladeSizes_Response,
        error: loadAddToCartBladeSizes_Error
    });
}
function loadAddToCartBladeSizes_Response(data) {
    if (data.Success) {
        $("#addtocart_bladesize").scrollTop(0);
        $("#addtocart_bladesize").find("option").remove().end();
        if (data.Data != null) {
            $.each(data.Data, function () {
                $("#addtocart_bladesize").append('<option value="' + this + '">' + this + '</option>');
            });
        }
        selectFirstOption("addtocart_bladesize");
    } else {
        $("#admin_alert_msg").text(data.Data);
        $("#dlg_admin_alert").dialog("open");
    }
}
function loadAddToCartBladeSizes_Error(xhr) {
    showAjaxErrorMessage("loadAddToCartBladeSizes_Error", xhr.status, xhr.response);
}

// *** loadAddToCartBladeDetails (Request/Response/Error) ***
function loadAddToCartBladeDetails_Request(make, model, size) {
    $("#addtocart_detail").text = "";
    var qs = { make: make, model: model, size: size };
    var url = "/HomeJson/CutPointsDetails";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: qs,
        success: loadAddToCartBladeDetails_Response,
        error: loadAddToCartBladeDetails_Error
    });
}
function loadAddToCartBladeDetails_Response(data) {
    if (data.Success) {
        if (data.Data != null) {
            if (data.Data.AdditionalInfo.length > 0) {
                $("#addtocart_detail").text(data.Data.CutPoint1 + " " + data.Data.AdditionalInfo);
            } else {
                $("#addtocart_detail").text(data.Data.CutPoint1);
            }
        }
    } else {
        $("#admin_alert_msg").text(data.Data);
        $("#dlg_admin_alert").dialog("open");
    }
}
function loadAddToCartBladeDetails_Error(xhr) {
    showAjaxErrorMessage("loadAddToCartBladeDetails_Error", xhr.status, xhr.response);
}
