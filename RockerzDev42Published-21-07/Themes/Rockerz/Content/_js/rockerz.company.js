﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />
/// <reference path="~/Scripts/rockerz.common.js" />

$(document).ready(function () {
    $("#company_tabs").tabs();
    var whichtab = getParameterByName("t");
    if (whichtab === "c") {
        $("#company_tabs").tabs("option", "selected", 1);
    }
});